﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.IO;
using System.Threading.Tasks;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService, IDisposable
    {
        public LogService()
        {
            this.LogPath = Settings.LogPath;

        }
        public LogService(string logPath)
        {
            this.LogPath = logPath;
        }
        public string LogPath { get; set; }

        public void Dispose()
        {
            //File.Delete(LogPath); 
        }

        public string Read()
        {
            string textFromFile = "";
            try
            {
                textFromFile = File.ReadAllText(LogPath);
            }
            catch (FileNotFoundException)
            {
                throw new InvalidOperationException();
            }

            return textFromFile;
        }

        public async void Write(string logInfo)
        {
            using StreamWriter sw = new StreamWriter(LogPath, true, System.Text.Encoding.Default);
            
            if (logInfo is not null)
            {
                await sw.WriteLineAsync(logInfo);
            }
            
        }
    }
}