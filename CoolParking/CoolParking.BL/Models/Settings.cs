﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal InitialBalance { get; } = 0;
        public static int MaxCapacity { get; set; } = 10;
        public static int PaymentPerid { get; } = 5; //in seconds
        public static int LogPerid { get; } = 60; //in seconds 
        public static decimal Coefficient { get; } = 2.5m;

        public static Dictionary<VehicleType, decimal> VehiclesTariff { get; }
        static Settings()
        {
            VehiclesTariff = new Dictionary<VehicleType, decimal>
            {
                {VehicleType.PassengerCar, 2m },
                {VehicleType.Truck, 5m },
                {VehicleType.Bus, 3.5m },
                {VehicleType.Motorcycle, 1m }
            };
        }
        public static string LogPath { get; } = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
    }
}