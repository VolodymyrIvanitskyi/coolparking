﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Services
{
    public class UserService
    {
        private HttpClient _client;

        public UserService()
        {
            _client = new HttpClient();
        }

        public async Task<List<User>> GetUsers()
        {
            var users = await _client.GetStringAsync("https://jsonplaceholder.typicode.com/users");
            return JsonConvert.DeserializeObject<List<User>>(users);
        }

        public async Task<User> GetUser(int id)
        {
            var user = await _client.GetStringAsync($"https://jsonplaceholder.typicode.com/users/{id}");
            return JsonConvert.DeserializeObject<User>(user);
        }
    }

    public class User
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("username")]
        public string Username { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("website")]
        public string Website { get; set; }
    }
}
