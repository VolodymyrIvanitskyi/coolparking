﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using CoolParking.BL.Services;

namespace CoolParking.BL.Menu
{
    class MenuByHttp
    {
        private HttpClient _client;
        public MenuByHttp()
        {
            _client = new HttpClient();
        }

        public void BasicMenu()
        {
            Console.WriteLine();
            Console.WriteLine("1 - show parking balance");
            Console.WriteLine("2 - show income by last minute");
            Console.WriteLine("3 - count of free parking spaces");
            Console.WriteLine("4 - show transactions by last minute");
            Console.WriteLine("5 - show all transactions");
            Console.WriteLine("6 - show all vehicles on the parking ");
            Console.WriteLine("7 - add new vehicle to parking");
            Console.WriteLine("8 - remove vehicle from parking");
            Console.WriteLine("9 - add balance for vehicle");
            Console.WriteLine("q - Exit \n");
        }

        //1
        public async void ShowBalance()
        {
            var response = await _client.GetAsync("https://localhost:44334/api/parking/balance");
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                decimal balance = JsonConvert.DeserializeObject<decimal>(json);
                Console.WriteLine($"Parking balance: {balance}");
            }
            else
            {
                Console.WriteLine($"Error. Status code: {response.StatusCode}");
            }
        }

        //2
        public async void ShowIncomeByLastMinute()
        {
            var response = await _client.GetAsync("https://localhost:44334/api/transactions/last");
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                var transactionInfos = JsonConvert.DeserializeObject<List<TransactionInfo>>(json);
                decimal incomeByLastMinute = transactionInfos.Sum(t => t.Sum);
                Console.WriteLine($"Income by last minute: {incomeByLastMinute}");
            }
            else
            {
                Console.WriteLine($"Error. Status code: {response.StatusCode}");
            }
        }

        //3
        public async void ShowFreeParkingPlaces()
        {
            var response = await _client.GetAsync("https://localhost:44334/api/parking/freePlaces");
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                int freePlaces = JsonConvert.DeserializeObject<int>(json);
                Console.WriteLine($"Free {freePlaces} places out of {Settings.MaxCapacity}");
            }
            else
            {
                Console.WriteLine($"Error. Status code: {response.StatusCode}");
            }
        }

        //4
        public async void ShowTransactionByLastMinute()
        {
            var response = await _client.GetAsync("https://localhost:44334/api/transactions/last");

            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                var transactionInfos = JsonConvert.DeserializeObject<List<TransactionInfo>>(json);

                foreach (var t in transactionInfos)
                {
                    Console.WriteLine($"Vehicle Id: {t.VehicleId} {t.Operation} sum: {t.Sum}, time: {t.Time}");
                }
            }
            else
            {
                Console.WriteLine($"Error. Status code: {response.StatusCode}");
            }
        }

        //5
        public async void ShowAllTransactions()
        {
            var response = await _client.GetAsync("https://localhost:44334/api/transactions/all");
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();

                var transactions = JsonConvert.DeserializeObject<string>(json);

                Console.WriteLine("All transactions: ");
                Console.WriteLine(transactions);
            }
            else
            {
                Console.WriteLine("Transaction log history is empty or log file is not found");
                Console.WriteLine($"Status code {response.StatusCode}");
            }
        }

        //6
        public async void ShowAllVehicles()
        {
            var response = await _client.GetAsync("https://localhost:44334/api/vehicles");
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();

                var vehicles = JsonConvert.DeserializeObject<List<Vehicle>>(json);

                foreach (var vehicle in vehicles)
                {
                    Console.WriteLine($"Id: {vehicle.Id}, type: {vehicle.VehicleType}, balance: {vehicle.Balance}");
                }
            }
            else
            {
                Console.WriteLine($"Error. Status code: {response.StatusCode}");
            }
        }

        //7
        public async void AddNewVehicle()
        {
            Console.WriteLine("Select type of the vehicle or input q back to main menu:");
            Console.WriteLine("1 - Passenger");
            Console.WriteLine("2 - Truck");
            Console.WriteLine("3 - Bus");
            Console.WriteLine("4 - Motorcycle\n");

            var input = Console.ReadLine();
            int type;
            if (int.TryParse(input, out int result))
                type = Convert.ToInt32(input);
            else if (input == "q")
                return;
            else
            {
                Console.WriteLine("The value should be between 1 and 4! ");
                return;
            }

            Console.WriteLine("Enter ID of the vehicle, format XX-YYYY-XX (where X is any uppercase letter of the English alphabet, and Y is any number)");
            string id = Console.ReadLine();
            
            Console.WriteLine("Enter balance:");

            var balanceInput = Console.ReadLine();
            decimal balance;

            if (decimal.TryParse(input, out decimal b))
                balance = Convert.ToDecimal(balanceInput);
            else
            {
                Console.WriteLine("Write a number!");
                return;
            }

            Vehicle vehicle = null;
            try
            {
                vehicle = new Vehicle(id, (VehicleType)type, balance);
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Vehicle body is invalid");
                return;
            }

            string uri = "https://localhost:44334/api/vehicles";
            var json = JsonConvert.SerializeObject(vehicle, Formatting.Indented);
            var contex = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(uri, contex);

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Vehicle added successfully");
            }
            else if(response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                Console.WriteLine($"Body si invalid");
            }
        }


        //8
        public async void RemoveVehicle()
        {
            Console.WriteLine("Input id of the vehicle or input q back to main menu");
            string input = Console.ReadLine();
            if (input == "q")
                return;

            var response = await _client.DeleteAsync($"https://localhost:44334/api/vehicles/{input}");
            if(response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                Console.WriteLine($"Vehicle removed successfully");
            }
            else if(response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                Console.WriteLine($"Vehicle id is invalid");
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                Console.WriteLine($"Vehicle not found");
            }

        }

        //9
        public async void TopUpBalance()
        {
            Console.WriteLine("Input id of the vehicle or input q back to main menu");
            string input = Console.ReadLine();
            if (input == "q")
                return;

            Console.WriteLine("Input balance or input q back to main menu");
            var balanceInput = Console.ReadLine();
            decimal balance;
            if (decimal.TryParse(balanceInput, out decimal result))
            {
                balance = Convert.ToDecimal(balanceInput);
            }
            else
            {
                Console.WriteLine("Write a number!");
                return;
            }

            VehicleTransactionsDTO vehicle = new VehicleTransactionsDTO();
            vehicle.Id = input;
            vehicle.Sum = balance;

            string uri = "https://localhost:44334/api/transactions/topUpVehicle";
            var contex = new StringContent(JsonConvert.SerializeObject(vehicle, Formatting.Indented), Encoding.UTF8, "application/json");

            var response = await _client.PutAsync(uri, contex);
            if (response.IsSuccessStatusCode)
                Console.WriteLine("Vehicle balance topped up successfully");
            else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                Console.WriteLine($"Body is invalid (sum must be greater than zero)");
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                Console.WriteLine($"Vehicle not found");
            }
        }

        public void IncorrectInput()
        {
            Console.WriteLine("Incorect input. Please enter the correct data");
        }

    }
}
