﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public struct TransactionInfo
    {
        public TransactionInfo(string operation, decimal sum, DateTime dateTime, string vehicleId)
        {
            Sum = sum;
            Time = dateTime;
            VehicleId = vehicleId;
            Operation = operation;
        }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }
        [JsonProperty("transactionDate")]
        public DateTime Time { get; set; }
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }
        [JsonProperty("operation")]
        public string Operation { get; set; }
    }
}