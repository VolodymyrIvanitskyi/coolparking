﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public struct TransactionInfoDTO
    {
        public TransactionInfoDTO(string operation, decimal sum, DateTime dateTime, string vehicleId)
        {
            Sum = sum;
            Time = dateTime;
            VehicleId = vehicleId;
            Operation = operation;
        }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }
        [JsonProperty("time")]
        public DateTime Time { get; set; }
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }
        [JsonProperty("operation")]
        public string Operation { get; set; }
    }
}
