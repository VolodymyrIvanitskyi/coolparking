﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoolParking.BL.Services
{
    public static class ValidationHelper
    {
        public static bool IsValidId(string id)
        {
            string pattern = "^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$";
            return Regex.IsMatch(id, pattern);
        }
    }
}

