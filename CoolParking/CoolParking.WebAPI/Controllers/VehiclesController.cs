﻿using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Models;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/vehicles")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        public VehiclesController()
        {
        }

        [HttpGet]
        public ActionResult<List<Vehicle>> Get()
        {
            return Ok(JsonConvert.SerializeObject(ParkingService.Instance.GetVehicles(),Formatting.Indented));

        }

        [HttpGet("{id}")]
        public ActionResult<Vehicle> Get(string id)
        {
            try
            {
                return Ok(JsonConvert.SerializeObject(ParkingService.Instance.GetVehicle(id),Formatting.Indented));
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
            catch(InvalidOperationException)
            {
                return NotFound();
            }
        }


        [HttpPost]
        public ActionResult Post([FromBody] VehicleDTO vehicle)
        {
            Vehicle _vehicle = null;

            try
            {
                _vehicle = new Vehicle(vehicle.Id, vehicle.VehicleType, vehicle.Balance);
                ParkingService.Instance.AddVehicle(_vehicle);
                return new ObjectResult(_vehicle) { StatusCode = StatusCodes.Status201Created };
                //return Created("",_vehicle);
            }
            catch(ArgumentException)
            {
                return BadRequest();
            }
        }
        
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            try
            {
                ParkingService.Instance.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
            catch (InvalidOperationException)
            {
                return BadRequest();
            }
        }
    }
}
