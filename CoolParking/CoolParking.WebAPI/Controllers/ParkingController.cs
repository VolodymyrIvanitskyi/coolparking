﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Services;
using Newtonsoft.Json;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    public class ParkingController : ControllerBase
    {
        public ParkingController()
        {
        }

        [HttpGet]
        [Route("api/parking/balance")]
        public ActionResult<decimal> Balance()
        {
            return Ok(JsonConvert.SerializeObject(ParkingService.Instance.GetBalance(), Formatting.Indented));
        }
        
        [HttpGet]
        [Route("api/parking/capacity")]
        public ActionResult<int> Capacity()
        {
            return Ok(JsonConvert.SerializeObject(ParkingService.Instance.GetCapacity(),Formatting.Indented));
        }

        
        [HttpGet]
        [Route("api/parking/freePlaces")]
        public ActionResult<int> FreePlaces()
        {
            return Ok(JsonConvert.SerializeObject(ParkingService.Instance.GetFreePlaces(), Formatting.Indented));
        }
        
    }
}
