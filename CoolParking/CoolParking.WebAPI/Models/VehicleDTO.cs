﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public class VehicleDTO
    {
        [JsonProperty("id")]
        [JsonRequired]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        [JsonRequired]
        public VehicleType VehicleType { get; set; }

        [JsonProperty("balance")]
        [JsonRequired]
        public decimal Balance { get; set; }

        public VehicleDTO(string id, VehicleType vehicleType, decimal balance)
        {
            this.Id = id;
            this.Balance = balance;
            this.VehicleType = vehicleType;
        }
    }
}
