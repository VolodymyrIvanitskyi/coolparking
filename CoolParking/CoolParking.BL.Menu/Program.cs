﻿using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;


namespace CoolParking.BL.Menu
{
    class Program
    {
        static void Main(string[] args)
        {
            //BSA21 task2 .Net cosystem

            /*ParkingService.Instance.AddVehicle(new Vehicle("AA-1111-AA",VehicleType.Motorcycle, 150));
            ParkingService.Instance.AddVehicle(new Vehicle("BB-2222-BB",VehicleType.PassengerCar, 200));

            ParkingService.Instance.StartWorking();

            //Menu menu = new Menu(); 
            */

            MenuByHttp menu = new MenuByHttp();

            while (true)
            {
                menu.BasicMenu();

                var input = Console.ReadKey();
                Console.WriteLine();
                switch (input.KeyChar)
                {
                    case '1':
                        menu.ShowBalance();
                        break;
                    case '2':
                        menu.ShowIncomeByLastMinute();
                        break;
                    case '3':
                        menu.ShowFreeParkingPlaces();
                        break;
                    case '4':
                        menu.ShowTransactionByLastMinute();
                        break;
                    case '5':
                        menu.ShowAllTransactions();
                        break;
                    case '6':
                        menu.ShowAllVehicles();
                        break;
                    case '7':
                        menu.AddNewVehicle();
                        break;
                    case '8':
                        menu.RemoveVehicle();
                        break;
                    case '9':
                        menu.TopUpBalance();
                        break;
                    case 'q':
                        Console.Clear();
                        break;
                    
                    default:
                        menu.IncorrectInput();
                        break;
                }
            }
        }
    }
}
