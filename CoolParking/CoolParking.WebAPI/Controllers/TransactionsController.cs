﻿using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        //private readonly ParkingService _parkingService;

        public TransactionsController(/*ParkingService parkingService*/)
        {
            //_parkingService = parkingService;
        }

        [HttpGet]
        [Route("api/transactions/last")]
        public ActionResult<List<TransactionInfo>> Last()
        {
            return Ok(JsonConvert.SerializeObject(
                ParkingService.Instance.GetLastParkingTransactions().ToList(),
                Formatting.Indented));
        }

        [HttpGet]
        [Route("api/transactions/all")]
        public ActionResult<List<TransactionInfo>> All()
        {
            try
            {
                return Ok(JsonConvert.SerializeObject(
                    ParkingService.Instance.ReadFromLog(),
                    Formatting.Indented));
            }
            catch(InvalidOperationException)
            {
                return NotFound();
            }
        }

        [HttpPut]
        [Route("api/transactions/topUpVehicle")]
        public ActionResult Put([FromBody] VehicleTransactionsDTO transactionsDTO)
        {
            try
            {
                ParkingService.Instance.TopUpVehicle(transactionsDTO.Id, transactionsDTO.Sum);
                return Ok();
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
            catch(InvalidOperationException)
            {
                return BadRequest();
            }
        }
    }
}
