﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public static decimal Balance { get; internal set; }
        public static List<Vehicle> Vehicles { get; set; }
        public static int MaxCapacity { get; private set; }
        static Parking()
        {
            Balance = Settings.InitialBalance;
            Vehicles = new List<Vehicle>();
            MaxCapacity = Settings.MaxCapacity;
        }
    }
}