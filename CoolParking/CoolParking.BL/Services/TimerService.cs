﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer timer;
        public TimerService()
        {
            timer = new Timer();
        }
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            timer.Stop();
            timer.Dispose();
        }

        public void Start()
        {
            timer.Interval = Interval*1000;
            timer.Elapsed += Timer1_Elapsed;
            timer.Start();
        }

        private void Timer1_Elapsed(object sender, ElapsedEventArgs e)
        {
            Elapsed?.Invoke(this, null);
        }

        public void Stop()
        {
            timer.Stop();
        }
    }
}