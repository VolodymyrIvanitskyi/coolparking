﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using CoolParking.BL.Services;
using Newtonsoft.Json;
using System;

namespace CoolParking.BL.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Vehicle
    {
        [JsonProperty("id")]
        [JsonRequired]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        [JsonRequired]
        public VehicleType VehicleType { get; set; }

        [JsonProperty("balance")]
        [JsonRequired]
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (ValidationHelper.IsValidId(id) && balance >= 0 && ((int)vehicleType>=1 && (int)vehicleType<=4))
            {
                this.Id = id;
                this.Balance = balance;
                this.VehicleType = vehicleType;
            }
            else throw new ArgumentException();
        }

        public  static string GenerateRandomRegistrationPlateNumber()
        {
            Random random = new Random();

            char randomChar1 = (char)random.Next('A', 'Z');
            char randomChar2 = (char)random.Next('A', 'Z');
            char randomChar3 = (char)random.Next('A', 'Z');
            char randomChar4 = (char)random.Next('A', 'Z');

            int digit1 = random.Next(0, 9);
            int digit2 = random.Next(0, 9);
            int digit3 = random.Next(0, 9);
            int digit4 = random.Next(0, 9);
            return $"{randomChar1}{randomChar2}-{digit1}{digit2}{digit3}{digit4}-{randomChar3}{randomChar4}";
        }
    }
}