﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using CoolParking.BL.Models;

namespace CoolParking.BL.Menu
{
    class Menu
    {
        /*private readonly ParkingService _parkingService;
        public Menu(ParkingService parkingService)
        {

            _parkingService = parkingService;
        }*/

        public void BasicMenu()
        {
            Console.WriteLine();
            Console.WriteLine("1 - show parking balance");
            Console.WriteLine("2 - show income by last minute");
            Console.WriteLine("3 - count of free parking spaces");
            Console.WriteLine("4 - show transactions by last minute");
            Console.WriteLine("5 - show all transactions");
            Console.WriteLine("6 - show all vehicles on the parking ");
            Console.WriteLine("7 - add new vehicle to parking");
            Console.WriteLine("8 - remove vehicle from parking");
            Console.WriteLine("9 - add balance for vehicle");
            Console.WriteLine("q - Exit \n");
        }

        //1
        public void ShowBalance()
        {
            Console.WriteLine($"Parking balance: {ParkingService.Instance.GetBalance()}");
        }

        //2
        public void ShowIncomeByLastMinute()
        {
            var transactionsByLastMinute = ParkingService.Instance.GetLastParkingTransactions();
            decimal incomeByLastMinute = transactionsByLastMinute.ToList().Sum(t => t.Sum);
            Console.WriteLine($"Income by last minute: {incomeByLastMinute}");
        }

        //3
        public void ShowFreeParkingPlaces()
        {
            Console.WriteLine($"Free {ParkingService.Instance.GetFreePlaces()} places out of {Settings.MaxCapacity}");
        }

        //4
        public void ShowTransactionByLastMinute()
        {
            var transactions = ParkingService.Instance.GetLastParkingTransactions();
            foreach(var t in transactions)
            {
                Console.WriteLine($"Vehicle Id: {t.VehicleId}, sum: {t.Sum}, time: {t.Time}");
            }
        }

        //5
        public void ShowAllTransactions()
        {
            string output = "";
            try
            {
                output = ParkingService.Instance.ReadFromLog();
                Console.WriteLine("All transactions: ");
                Console.WriteLine(output);
            }
            catch(InvalidOperationException)
            {
                Console.WriteLine("Transaction log history is empty");
            }
        }

        //6
        public void ShowAllVehicles()
        {
            var vehicles = ParkingService.Instance.GetVehicles();

            foreach (var vehicle in vehicles)
            {
                Console.WriteLine($"Id: {vehicle.Id}, type: {vehicle.VehicleType}, balance: {vehicle.Balance}");
            }
        }

        //7
        public void AddNewVehicle()
        {
            Console.WriteLine("Select type of the vehicle or input q back to main menu:");
            Console.WriteLine("1 - Passenger");
            Console.WriteLine("2 - Truck");
            Console.WriteLine("3 - Bus");
            Console.WriteLine("4 - Motorcycle\n");

            var input = Console.ReadLine();
            int type = 0;
            if(int.TryParse(input, out int result))
            {
                type = Convert.ToInt32(input);
            }
            else if(input =="q")
            {
                return;
            }
            else
            {
                Console.WriteLine("The value should be between 1 and 4! ");
                return;
            }

            Console.WriteLine("Enter ID of the vehicle, format XX-YYYY-XX (where X is any uppercase letter of the English alphabet, and Y is any number)");
            string id = Console.ReadLine();

            
            if (type > 0 && type <= 4)
            {
                if (ValidationHelper.IsValidId(id))
                {
                    Vehicle vehicle = new Vehicle(id, (VehicleType)type, 0m);
                    ParkingService.Instance.AddVehicle(vehicle);
                    Console.WriteLine("Vehicle added successfully");
                }
                else
                {
                    Console.WriteLine("Wrong format of the id");
                    return;
                }
            }
            else
            {
                Console.WriteLine("The value should be between 1 and 4! ");
                return;
            }
        }

        //8
        public void RemoveVehicle()
        {
            Console.WriteLine("Input id of the vehicle or input q back to main menu");
            string input = Console.ReadLine();
            if (input == "q")
                return;
            else if(ValidationHelper.IsValidId(input))
            {
                try
                {
                    ParkingService.Instance.RemoveVehicle(input);
                    Console.WriteLine("Vehicle removed successfully");
                }
                catch(InvalidOperationException)
                {
                    Console.WriteLine("Can`t remove vehicle with  negative balance;");
                }
                catch(ArgumentException)
                {
                    Console.WriteLine("Can`t remove vehicle, because vehicle doen`t exist");
                }
            }
            else
                Console.WriteLine("Invalid id");
        }

        //9
        public void TopUpBalance()
        {
            Console.WriteLine("Input id of the vehicle or input q back to main menu");
            string input = Console.ReadLine();
            if (input == "q")
                return;
            else if (ValidationHelper.IsValidId(input))
            {
                Console.WriteLine("Input balance or input q back to main menu");
                decimal balance = Convert.ToDecimal(Console.ReadLine());

                try
                {
                    ParkingService.Instance.TopUpVehicle(input, balance);
                    Console.WriteLine("Balance topped up");
                }
                catch(ArgumentException)
                {
                    Console.WriteLine("Incorrect input parameters");
                }
            }
            else
            {
                Console.WriteLine("Invalid ID");
            }
        }

        public void IncorrectInput()
        {
            Console.WriteLine("Incorect input. Please enter the correct data");
        }

        public void ParkingStopWorking()
        {
            ParkingService.Instance.Dispose();
        }

    }
}
