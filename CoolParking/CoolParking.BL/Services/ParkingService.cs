﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using System;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;
using CoolParking.BL.Models;
using System.IO;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService,IDisposable
    {
        private readonly ITimerService withdrawTimer;
        private readonly ITimerService logTimer;
        private readonly ILogService logService;
        private readonly List<TransactionInfo> transactionInfo;

        private static ParkingService instance = new ParkingService();
 
        public static ParkingService Instance { get { return instance; } }

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            this.withdrawTimer = withdrawTimer;
            this.logTimer = logTimer;
            this.logService = logService;
            transactionInfo = new List<TransactionInfo>();

            withdrawTimer.Interval = Settings.PaymentPerid;
            withdrawTimer.Elapsed += WithdrawTimer_Elapsed;

            logTimer.Interval = Settings.LogPerid;
            logTimer.Elapsed += LogTimer_Elapsed;
        }

        public ParkingService()
        {
            this.withdrawTimer = new TimerService();
            this.logTimer = new TimerService();
            this.logService = new LogService();
            transactionInfo = new List<TransactionInfo>();

            withdrawTimer.Interval = Settings.PaymentPerid;
            withdrawTimer.Elapsed += WithdrawTimer_Elapsed;

            logTimer.Interval = Settings.LogPerid;
            logTimer.Elapsed += LogTimer_Elapsed;

            //Parking.Vehicles.Add(new Vehicle("AA-1111-AA", VehicleType.Bus, 200m));
            //Parking.Vehicles.Add(new Vehicle("BB-2222-BB", VehicleType.Truck, 200m));
        }

        public void StartWorking()
        {
            withdrawTimer.Start();
            logTimer.Start();
        }
        private void LogTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            logService.Write($"Starting loggining: {DateTime.Now}");
            foreach (var transaction in transactionInfo)
            {
                logService.Write("Time: "+transaction.Time + ", vehicleId: " + transaction.VehicleId + ", operation: "+transaction.Operation+ ", sum: " + transaction.Sum);
            }
            transactionInfo.Clear();
        }

        private void WithdrawTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            foreach (var vehicle in Parking.Vehicles)
            {
                DebitMoney(vehicle);
            }
        }

        private void DebitMoney(Vehicle vehicle)
        {
            decimal tariff = Settings.VehiclesTariff[vehicle.VehicleType];

            decimal withdrawedSum = 0m;
            if (vehicle.Balance < tariff)
            {
                if (vehicle.Balance > 0)
                {
                    decimal diff = tariff - vehicle.Balance;
                    withdrawedSum += vehicle.Balance;
                    withdrawedSum += diff * Settings.Coefficient;
                }
                else
                {
                    withdrawedSum += tariff * Settings.Coefficient;
                }
            }
            else
            {
                withdrawedSum += tariff;
            }

            TransactionInfo transaction = new TransactionInfo("Withdraw",withdrawedSum, DateTime.Now, vehicle.Id);
            Parking.Balance += transaction.Sum;
            vehicle.Balance -= withdrawedSum;

            transactionInfo.Add(transaction);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() > 0)
            {
                foreach (var v in Parking.Vehicles)
                {
                    if (v.Id == vehicle.Id)
                        throw new ArgumentException(); //If vehicle with current id exists  
                }
                Parking.Vehicles.Add(vehicle);
            }
            else
                throw new InvalidOperationException();
        }

        public void Dispose()
        {
            logTimer.Dispose();
            withdrawTimer.Dispose();
            transactionInfo.Clear();
            Parking.Vehicles.Clear();
            Parking.Balance = Settings.InitialBalance;
            //File.Delete(Settings.LogPath);
            GC.SuppressFinalize(this);
        }

        public decimal GetBalance()
        {
            return Parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.MaxCapacity;
        }

        public int GetFreePlaces()
        {
            return Settings.MaxCapacity - Parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactionInfo.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return Parking.Vehicles.AsReadOnly();
        }

        public Vehicle GetVehicle(string id)
        {
            if(!ValidationHelper.IsValidId(id))
            {
                throw new ArgumentException();
            }

            var _vehicle = Parking.Vehicles.FirstOrDefault(v => v.Id == id);

            if (_vehicle is not null)
            {
                return _vehicle;
            }
            else
                throw new InvalidOperationException();
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var _vehicle = Parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);
            if (!ValidationHelper.IsValidId(vehicleId))
            {
                throw new InvalidOperationException();
            }

            if (_vehicle is not null)
            {
                if (_vehicle.Balance >= 0)
                    Parking.Vehicles.Remove(_vehicle);
                else
                    throw new InvalidOperationException();
            }
            else
                throw new ArgumentException();
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (!ValidationHelper.IsValidId(vehicleId))
            {
                throw new InvalidOperationException("Invalid ID");
            }

            var vehicle = Parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);
            if (vehicle is not null)
            {
                if (sum >= 0)
                    vehicle.Balance += sum;
                else
                    throw new InvalidOperationException("Sum must be greatest than zero");
            }
            else
                throw new ArgumentException("Vehicle not found");

            TransactionInfo transaction = new TransactionInfo("Top Up:", sum, DateTime.Now,vehicleId);
            transactionInfo.Add(transaction);
        }

    }
}